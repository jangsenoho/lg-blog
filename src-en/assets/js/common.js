$(function () {
    /*search box*/
    $('.search button').on('click', function () {
        $('.search-box').addClass('active');
        $('body').css('overflow', 'hidden');
        $('.input-box input').focus();
        $('.close').on('click', function () {
            $('.search-box').removeClass('active');
            $('body').css('overflow', 'initial');
        })
    })
    $('.welcome-banner .text-banner a').on('click', function () {
        $('.popup-wrap').addClass('on');
        $('body').css('overflow', 'hidden');
        $('.popup-wrap .close').on('click', function () {
            $('.popup-wrap').removeClass('on');
            $('body').css('overflow', 'initial');
        })
    })
    /*topbutton*/
    var btn = $('.top-button');
    $(window).scroll(function () {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });
    btn.on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, '300');
    });
    /*family site*/
    $('.site-area > button').on('click', function () {
        $('.site-list').toggleClass('active');
        $(this).toggleClass('on');
        $('.site-list > a').on('click', function () {
            $('.site-area > button').removeClass('on');
            $('.site-list').removeClass('active');
        })
    })
    /*2depth tab menu*/
    $(function () {
        $('.tit ul li').first().addClass('on');
        $('.comu .list-box .list,.storage-box div').not(':first').hide();
          $('.tit ul li').on('click', function () {
            $(this).addClass('on').siblings().removeClass('on');
            var Cont = $(this).index();
            $('.comu .list-box .list,.storage-box div').hide();
            $('.comu .list-box .list,.storage-box div').eq(Cont).show();
            if(!$(this).is('#all-button')){
                var cont = $(this).index() - 1;
                $('.news .list-box .list,.search-accordion > div').hide();
                $('.news .list-box .list,.search-accordion > div').eq(cont).show();
                // $('.ensole-wrap .ensole-box').eq(1).css('margin-top','3.75rem');
            }else{
                $('.news .list-box .list,.search-accordion > div').show();
                // $('.ensole-wrap .ensole-box').eq(1).css('margin-top','0');              
            }

        })
    })
    /*news*/
    $(function () {
        $(".list li").on("mouseover", function () {
            $(this).addClass("active");
        }).on("mouseleave", function () {
            $(this).removeClass("active");
        })
    })
    /*people*/
    // $(function () {
    //     var updateMasonry = function () {
    //         $('.ensole-wrap').masonry({
    //             itemSelector: '.item',
    //             columnWidth: '.item',
    //             gutter: 20,
    //             horizontalOrder: true,
    //             percentPosition: true
    //         })
    //     }
    //     updateMasonry();
    //     $(window).on('resize load', updateMasonry);
    //     $('.tit ul li').on('click', updateMasonry);
    // })
    $(function () {
        var updateMasonry = function () {
            $('#ensole-box').masonry({
                itemSelector: '.item',
                columnWidth: 264,
                gutter: 20,
                horizontalOrder: true,
                percentPosition: true
            })
        }
        updateMasonry();
        $(".tit ul li").click(function() {
            var group = $(this).data('category');
            var group_class = "." + group;
            console.log(group_class);
            if(group == "*"){
                $(".item").show();
                $("#ensole-box").masonry()
            }
            else if(group != "") {
                $(".item").hide();
                $(group_class).show();
                $("#ensole-box").masonry()
            } else {
                $(".item").show();
                $("#ensole-box").masonry()
            }
        });
        $(window).on('resize load', function(){
            updateMasonry();
            if(window.innerWidth < 1440){
                $('#ensole-box').css({'max-width':'1118px', 'margin-left':'auto', 'margin-right': 'auto'})
                updateMasonry();
            }else{ 
                $('#ensole-box').css({'max-width':'100%'})
                updateMasonry();
            }
        });
    })

    /*click event*/
    $(function () {
        $('.good-btn').on('click', function () {
            $(this).toggleClass('click');
        })
        $('.good-share').on('click', function () {
            $(this).toggleClass('in');
        })
        $('.good-area .rel-box').on('click',function(){
            $(this).parent().next().toggleClass('active')
        })
        $('.lang button').on('click',function(){
            $(this).next().toggleClass('in')
            $('a').on('click',function(){
                $(this).parent().removeClass('in');
            })
        })
    })

    //accordion
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
})
